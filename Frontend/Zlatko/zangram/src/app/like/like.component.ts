import { Component} from '@angular/core';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { Like } from '../models/like.model';
import { LikeService } from './like.service';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styles: []
})
export class LikeComponent {

  files: String[];
  like1Input: string;
  like1: string;

  constructor(private router: Router, private likeService: LikeService) {
    this.files = [];
    this.like1 = '';
  }

  listFiles(): void {
    this.like1 = this.like1Input;
    this.likeService.listFiles(this.like1).subscribe(files => {
      this.files = files as string[];
    })
  };

  getFile(like1, filename): void {
    this.likeService.getFile(like1, filename);
  }
}