import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Like } from '../models/like.model';
import { LikeService } from './like.service';

@Component({
  templateUrl: './add-like.component.html'
})
export class AddLikeComponent {
  
  selectedFiles: FileList
  like: Like = new Like();

  constructor(private router: Router, private likeService: LikeService) {

  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  createLike(): void {
    this.likeService.createLike(this.like, this.selectedFiles)
        .subscribe( data => {
          
        });
  };
}