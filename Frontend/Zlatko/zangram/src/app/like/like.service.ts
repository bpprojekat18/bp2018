import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LikeService {

  constructor(private http:HttpClient) {}

  private likeUrl = 'http://localhost:8080/likes/post';
	//private userUrl = '/api';

  public getLikes() {
    //return this.http.get<User[]>(this.userUrl);
  }

  listFiles(like1): Observable<string[]> {
    return this.http.get<string[]>('http://localhost:8080/likes/files/' + like1);
  }

  public createLike(like, files) {
    like.id = 99;
    let formdata: FormData = new FormData();
    formdata.append('formDataJson', JSON.stringify(like));
    for (var i = 0 ; i < files.length ; i++){
      formdata.append('files', files[i]);
    }
    const req = new HttpRequest('POST', 'http://localhost:8080/likes/post', formdata, {
      reportProgress: true
    });
 
    return this.http.request(req);
  }

  public getFile(like1, filename) {
    return this.http.post('http://localhost:8080/likes/files?like1=' + like1 + '&filename=' + filename, httpOptions).subscribe(res => {
      alert("File successfully downloaded.");
    });
  }
}
