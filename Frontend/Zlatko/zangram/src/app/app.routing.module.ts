import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserComponent } from './user/user.component';
import {AddUserComponent} from './user/add-user.component';

import { LocationComponent } from './location/location.component';
import {AddLocationComponent} from './location/add-location.component';

import { LikeComponent } from './like/like.component';
import {AddLikeComponent} from './like/add-like.component';

const routes: Routes = [
  { path: 'users', component: UserComponent },
  { path: 'add', component: AddUserComponent },
  { path: 'locations', component: LocationComponent },
  { path: 'addl', component: AddLocationComponent },
  { path: 'likes', component: LikeComponent },
  { path: 'add2', component: AddLikeComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }