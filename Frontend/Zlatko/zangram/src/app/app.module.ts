import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { AppRoutingModule } from './app.routing.module';
import {UserService} from './user/user.service';
import {HttpClientModule} from "@angular/common/http";
import {AddUserComponent} from './user/add-user.component';

import { LocationComponent } from './location/location.component';
import {LocationService} from './location/location.service';
import {AddLocationComponent} from './location/add-location.component';

import { LikeComponent } from './like/like.component';
import {LikeService} from './like/like.service';
import {AddLikeComponent} from './like/add-like.component';

@NgModule({
  declarations: [
    AppComponent,    UserComponent,
    AddUserComponent, LocationComponent, AddLocationComponent,
    LikeComponent, AddLikeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [UserService, LocationService, LikeService],
  bootstrap: [AppComponent],
})
export class AppModule { }