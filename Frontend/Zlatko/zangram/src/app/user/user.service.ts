import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  constructor(private http:HttpClient) {}

  private userUrl = 'http://localhost:8080/user/post';
	//private userUrl = '/api';

  public getUsers() {
    //return this.http.get<User[]>(this.userUrl);
  }

  listFiles(username): Observable<string[]> {
    return this.http.get<string[]>('http://localhost:8080/user/files/' + username);
  }

  public createUser(user, files) {
    user.id = 99;
    let formdata: FormData = new FormData();
    formdata.append('formDataJson', JSON.stringify(user));
    for (var i = 0 ; i < files.length ; i++){
      formdata.append('files', files[i]);
    }
    const req = new HttpRequest('POST', 'http://localhost:8080/user/post', formdata, {
      reportProgress: true
    });
 
    return this.http.request(req);
  }

  public getFile(username, filename) {
    return this.http.post('http://localhost:8080/user/files?username=' + username + '&filename=' + filename, httpOptions, {responseType: 'blob', observe: 'response'})
    .subscribe(res => {
        console.log('start download:',filename);
        var url = window.URL.createObjectURL(res.body);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = filename
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
}
