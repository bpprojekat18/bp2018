import { Component} from '@angular/core';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { User } from '../models/user.model';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: []
})
export class UserComponent {

  files: String[];
  usernameInput: string;
  username: string;

  constructor(private router: Router, private userService: UserService) {
    this.files = [];
    this.username = '';
  }

  listFiles(): void {
    this.username = this.usernameInput;
    this.userService.listFiles(this.username).subscribe(files => {
      this.files = files as string[];
    })
  };

  getFile(username, filename): void {
    this.userService.getFile(username, filename);
  }
}