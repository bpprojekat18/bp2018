import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../models/user.model';
import { UserService } from './user.service';

@Component({
  templateUrl: './add-user.component.html'
})
export class AddUserComponent {
  
  selectedFiles: FileList
  user: User = new User();

  constructor(private router: Router, private userService: UserService) {

  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  createUser(): void {
    this.userService.createUser(this.user, this.selectedFiles)
        .subscribe( data => {
          
        });
  };
}