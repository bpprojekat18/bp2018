import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Location } from '../models/location.model';
import { LocationService } from './location.service';

@Component({
  templateUrl: './add-location.component.html'
})
export class AddLocationComponent {
  
  selectedFiles: FileList
  location: Location = new Location();

  constructor(private router: Router, private locationService: LocationService) {

  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  createLocation(): void {
    this.locationService.createLocation(this.location, this.selectedFiles)
        .subscribe( data => {
          
        });
  };
}