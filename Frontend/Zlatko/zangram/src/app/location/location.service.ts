import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpRequest, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LocationService {

  constructor(private http:HttpClient) {}

  private locationUrl = 'http://localhost:8080/locations/post';
	//private userUrl = '/api';

  public getLocations() {
    //return this.http.get<User[]>(this.userUrl);
  }

  listFiles(place): Observable<string[]> {
    return this.http.get<string[]>('http://localhost:8080/locations/files/' + place);
  }

  public createLocation(location, files) {
    location.id = 99;
    let formdata: FormData = new FormData();
    formdata.append('formDataJson', JSON.stringify(location));
    for (var i = 0 ; i < files.length ; i++){
      formdata.append('files', files[i]);
    }
    const req = new HttpRequest('POST', 'http://localhost:8080/locations/post', formdata, {
      reportProgress: true
    });
 
    return this.http.request(req);
  }

  public getFile(place, filename) {
      return this.http.post('http://localhost:8080/locations/files?place=' + place + '&filename=' + filename, httpOptions, {responseType: 'blob', observe: 'response'})
      .subscribe(res => {
        console.log('start download:',filename);
        var url = window.URL.createObjectURL(res.body);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = filename
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      }, error => {
        console.log('download error:', JSON.stringify(error));
      }, () => {
        console.log('Completed file download.')
      });
  }
}
