import { Component} from '@angular/core';
import { Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { Location } from '../models/location.model';
import { LocationService } from './location.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styles: []
})
export class LocationComponent {

  files: String[];
  placeInput: string;
  place: string;

  constructor(private router: Router, private locationService: LocationService) {
    this.files = [];
    this.place = '';
  }

  listFiles(): void {
    this.place = this.placeInput;
    this.locationService.listFiles(this.place).subscribe(files => {
      this.files = files as string[];
    })
  };

  getFile(place, filename): void {
    this.locationService.getFile(place, filename);
  }
}