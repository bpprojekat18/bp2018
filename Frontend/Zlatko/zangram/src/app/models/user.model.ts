export class User {
  id: string;
  username: string;
  email: string;
  password: string;
  birthdate: string;
  fullname: string;
}