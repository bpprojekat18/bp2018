package com.zangram.zangram.service;

import com.zangram.zangram.model.Location;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface LocationService {
    public ArrayList<Location> getAllLocations();
    public Optional<Location> getLocation(long id);
    public Location save(Location location);
    public Optional<Location> findById(long id);
    public void addLog(String table, long locationId, long dataId, String job);
}
