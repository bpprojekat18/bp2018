package com.zangram.zangram.repositories;

import com.zangram.zangram.model.LocationVideo;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repository for crud operations for locationVideo table from database
 */
public interface LocationVideoRepository extends MongoRepository<LocationVideo, String> {
    LocationVideo findBy_id(ObjectId _id);
}

