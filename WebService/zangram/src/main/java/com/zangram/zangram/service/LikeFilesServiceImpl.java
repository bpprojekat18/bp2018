package com.zangram.zangram.service;

import com.zangram.zangram.model.LikeFiles;
import com.zangram.zangram.repositories.LikeFilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LikeFilesServiceImpl implements LikeFilesService {
    private LikeFilesRepository likeFilesRepository;

    @Autowired
    public LikeFilesServiceImpl(LikeFilesRepository likeFilesRepository) {
        this.likeFilesRepository = likeFilesRepository;
    }
    @Override
    public ArrayList<LikeFiles> getAllLikeFiles(long id) {
        ArrayList<LikeFiles> likeFiles = new ArrayList<>();
        ArrayList<LikeFiles> all = (ArrayList<LikeFiles>)likeFilesRepository.findAll();
        for (LikeFiles uf: all
        ) {
            if (uf.getLike_id() == id)
                likeFiles.add(uf);
        }
        return likeFiles;
    }

    @Override
    public LikeFiles save(long fileId, long likeId) {
        LikeFiles locfile = new LikeFiles(likeId, fileId);
        locfile.setId(1);
        return likeFilesRepository.save(locfile);
    }
}
