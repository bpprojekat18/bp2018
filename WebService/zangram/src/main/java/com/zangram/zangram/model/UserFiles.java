package com.zangram.zangram.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="user_files")
public class UserFiles implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_files_seq")
    @SequenceGenerator(name="user_files_seq", sequenceName="user_files_seq", allocationSize=1)
    private long id;

    @Column(name = "user_id")
    private long user_id;

    @Column(name = "file_id")
    private long file_id;

    public UserFiles() {
    }

    public UserFiles(long uid, long fid) {
        user_id = uid;
        file_id = fid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getFile_id() {
        return file_id;
    }

    public void setFile_id(long file_id) {
        this.file_id = file_id;
    }
}
