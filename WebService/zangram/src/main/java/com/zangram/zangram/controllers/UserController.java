package com.zangram.zangram.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.gridfs.GridFSDBFile;
import com.zangram.zangram.integrators.MetadataExtractorIntegrator;
import com.zangram.zangram.model.*;
import com.zangram.zangram.repositories.UserPhotoRepository;
import com.zangram.zangram.service.*;

import org.hibernate.boot.Metadata;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.mapping.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/user")
public class UserController {
    private UserService userService;
    private FileService fileService;
    private UserFilesService userFileService;

    private LocationService locationService;
    private LocationFilesService locationFileService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @Autowired
    public void setUserFileService(@Qualifier("userFilesServiceImpl") UserFilesService userFileService) {this.userFileService = userFileService; }

    @Autowired
    public void setProductService(LocationService locationService) {
        this.locationService = locationService;
    }

    @Autowired
    public void setLocationFileService(@Qualifier("locationFilesServiceImpl") LocationFilesService locationFileService) {this.locationFileService = locationFileService; }

    @Autowired
    private GridFsOperations gridFsOperations;

    @Autowired
    private UserPhotoRepository userPhotoRepository;

    @GetMapping("/jpa/metadata")
    public String getMetaData() throws JsonProcessingException, JSONException {
        Metadata metadata = MetadataExtractorIntegrator.INSTANCE.getMetadata();
        JSONArray tables = new JSONArray();
        for (PersistentClass persistentClass : metadata.getEntityBindings()) {
            JSONObject tableObject = new JSONObject();
            Table table = persistentClass.getTable();

            tableObject.put("className", persistentClass.getClassName());
            tableObject.put("tableName", table.getName());

            for(Iterator propertyIterator = persistentClass.getPropertyIterator();
                propertyIterator.hasNext(); ) {
                Property property = (Property) propertyIterator.next();
                JSONArray columns = new JSONArray();
                for(Iterator columnIterator = property.getColumnIterator();
                    columnIterator.hasNext(); ) {
                    JSONObject columnObject = new JSONObject();
                    Column column = (Column) columnIterator.next();
                    columnObject.put("property", property.getName());
                    columnObject.put("columnName", column.getName());
                    columnObject.put("columntType", column.getSqlType());
                    columns.put(columnObject);
                }
            }
            tables.put(tableObject);
        }
        return tables.toString();
    }

    /**
     *
     * @return Oracle database metadata as a JSON object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws JSONException
     */
    @GetMapping("/oracle/metadata")
    public String getOracleMetaData() throws SQLException, ClassNotFoundException, JSONException, IOException {
        Class.forName("oracle.jdbc.OracleDriver");
        Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@ora.db.lab.ri.etf.unsa.ba:1521:ETFLAB", "BP18", "RDnjyHMK");
        DatabaseMetaData metadata = connection.getMetaData();
        String[] types = {"TABLE"};
        ResultSet rs = metadata.getTables(null, "BP18", null, types);
        JSONArray tables = new JSONArray();
        while (rs.next()) {
            JSONObject obj = new JSONObject();
            obj.put("tableName", rs.getString(3));

            ResultSet primaryKeys = metadata.getPrimaryKeys(null, "BP18",  rs.getString(3));
            JSONArray pks = new JSONArray();
            while (primaryKeys.next())
                pks.put(primaryKeys.getString("COLUMN_NAME"));
            obj.put("primaryKeys", pks);

            ResultSet foreignKeys  = metadata.getExportedKeys(null, "BP18", rs.getString(3));
            JSONArray fks = new JSONArray();
            while (foreignKeys.next()) {
                JSONObject fk = new JSONObject();
                String fkTableName = foreignKeys.getString("FKTABLE_NAME");
                String fkColumnName = foreignKeys.getString("FKCOLUMN_NAME");
                int fkSequence = foreignKeys.getInt("KEY_SEQ");
                fk.put("fkTableName", fkTableName);
                fk.put("fkColumnName", fkColumnName);
                fk.put("fkSequence", fkSequence);
                fks.put(fk);
            }
            obj.put("foreignKeys", fks);
            ResultSet rs1 = metadata.getColumns(null, "BP18", rs.getString(3), null);
            JSONArray columns = new JSONArray();
            int brojac = 0;
            while (rs1.next()) {
                if (rs1.getString("COLUMN_NAME").toLowerCase().equals("id"))
                    if (brojac == 0)
                        brojac++;
                    else
                        break;
                JSONObject obj1 = new JSONObject();
                obj1.put("columnName", rs1.getString("COLUMN_NAME"));
                obj1.put("columnType", rs1.getString("TYPE_NAME"));
                obj1.put("columnSize", rs1.getString("COLUMN_SIZE"));
                columns.put(obj1);
            }
            obj.put("columns", columns);
            tables.put(obj);
        }
        JSONObject test = new JSONObject();
        test.put("tables", tables);

        types = new String[]{"SEQUENCE"};
        rs = metadata.getTables(null, "BP18", null, types);
        JSONArray sequences = new JSONArray();
        while (rs.next()) {
            JSONObject obj = new JSONObject();
            obj.put("sequenceName", rs.getString(3));
            sequences.put(obj);
        }
        test.put("sequences", sequences);

        types = new String[]{"TRIGGER"};
        rs = metadata.getTables(null, "BP18", null, types);
        JSONArray triggers = new JSONArray();
        while (rs.next()) {
            JSONObject obj = new JSONObject();
            obj.put("triggerName", rs.getString(3));
            triggers.put(obj);
        }
        test.put("triggers", triggers);
        types = new String[]{"PROCEDURE"};
        rs = metadata.getTables(null, "BP18", null, types);
        JSONArray procedures = new JSONArray();
        while (rs.next()) {
            JSONObject obj = new JSONObject();
            obj.put("procedureName", rs.getString(3));
            procedures.put(obj);
        }
        test.put("procedures", procedures);

        createDatabaseMySql(test);
        return test.toString();
    }

    /**
     *
     * @param oracleMetaData JSON object which represent metadata of oracle database
     * @throws SQLException
     * @throws JSONException
     * Create MySQL database base on Orracle database
     */
    private void createDatabaseMySql(JSONObject oracleMetaData) throws SQLException, JSONException, IOException {
        Connection conn = connect("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/bp2018?verifyServerCertificate=false&useSSL=true", "root", "root");
        JSONArray tables = oracleMetaData.getJSONArray("tables");
        for(int i = 0; i < tables.length(); i++) {
            JSONObject table = tables.getJSONObject(i);
            JSONArray columns = table.getJSONArray("columns");
            String query = "CREATE TABLE IF NOT EXISTS " + table.getString("tableName") + " (";
            for(int j = 0; j < columns.length(); j++) {
                String columnType = columns.getJSONObject(j).getString("columnType");
                String part2 = "";
                if (columnType.equals("NUMBER"))
                    part2 = " INT,";
                else if (columnType.equals("DATE"))
                    part2 = " DATE,";
                else if (columnType.equals("VARCHAR2"))
                    columnType = "VARCHAR";
                else if (columnType.equals("BLOB"))
                    part2 = "LONGBLOB,";
                if (part2.length() == 0)
                    part2 = columnType + "(" +
                            columns.getJSONObject(j).getString("columnSize") + "),";
                if (table.getString("tableName").toLowerCase().equals("file_") && columns.getJSONObject(j).getString("columnName").toLowerCase().equals("id"))
                    part2 = "VARCHAR(50),";
                query += columns.getJSONObject(j).getString("columnName") + " " + part2;
            }
            if (table.getJSONArray("primaryKeys").length() > 0) {
                String primaryKey = table.getJSONArray("primaryKeys").getString(0);
                query += "PRIMARY KEY (" + primaryKey + ")";
            } else
                query = query.substring(0, query.length() - 1);
            query += ");";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.execute();
        }
        migrateData(conn);
    }

    /**
     *
     * @param conn  connection MySQL database
     *              Migrate data from Oracle to MySQL database
     * @throws SQLException
     */
    @Transactional(rollbackFor = SQLException.class)
    void migrateData(Connection conn) throws SQLException, IOException {
        ArrayList<User> users = userService.getAllUsers();
        ArrayList<File> files = fileService.getAllFiles();
        ArrayList<UserFiles> userFiles = userFileService.getAll();
        ArrayList<Location> locations = locationService.getAllLocations();
        ArrayList<LocationFiles> locationFiles = locationFileService.getAll();

        try {
            conn.setAutoCommit(false);
            for (User user: users
            ) {
                String query = "INSERT INTO USER_ VALUES(?, ?, ?, ?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setLong(1, user.getId());
                ps.setString(2, user.getFullname());
                ps.setString(3, user.getUsername());
                ps.setString(4, user.getPassword());
                ps.setString(5, user.getEmail());
                ps.setDate(6, user.getBirthdate());
                ps.execute();
            }

            for (File file: files
            ) {
                String query = "INSERT INTO FILE_ VALUES(?, ?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setLong(1, file.getId());
                ps.setString(2, file.getFilename());
                ps.setString(3, file.getFiletype());
                ps.setBytes(4, file.getFile());
                //ps.setNull(4, 0);
                ps.execute();
            }

            for (UserFiles userFile: userFiles
            ) {
                String query = "INSERT INTO USER_FILES VALUES(?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setLong(1, userFile.getId());
                ps.setLong(2, userFile.getUser_id());
                ps.setLong(3, userFile.getFile_id());
                ps.execute();
            }

            for (Location location: locations
            ) {
                String query = "INSERT INTO LOCATION VALUES(?, ?)";
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setLong(1, location.getId());
                ps.setString(2, location.getPlace());
                ps.execute();
            }

            for (LocationFiles locationFile: locationFiles
            ) {
                String query = "INSERT INTO LOCATION_FILES VALUES(?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setLong(1, locationFile.getId());
                ps.setLong(2, locationFile.getLocation_id());
                ps.setLong(3, locationFile.getFile_id());
                ps.execute();
            }

            //nosql
            /*ArrayList<UserPhoto> userPhotos = userPhotoRepository.findAll();
            for(UserPhoto userPhoto: userPhotos)
            {
                GridFSDBFile dbFile = gridFsOperations.findOne(new Query(Criteria.where("_id").is(userPhoto.getPhotoId())));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                dbFile.writeTo(baos);
                byte[] bytes = baos.toByteArray();

                String query = "INSERT INTO FILE_ VALUES(?, ?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(query);
                ps.setString(1, userPhoto.getPhotoId());
                ps.setString(2, dbFile.getFilename());
                ps.setString(3, dbFile.get("format").toString());
                ps.setBytes(4, bytes);

                long fileId = -1;
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next())
                        fileId = generatedKeys.getLong(1);
                    else
                        throw new SQLException("Creating file failed, no ID obtained.");

                }

                query = "INSERT INTO USER_FILES VALUES(?, ?, ?)";
                ps = conn.prepareStatement(query);
                ps.setLong(1, 0);
                ps.setInt(2, Integer.parseInt(userPhoto.getUserId()));
                ps.setLong(3, fileId);
                ps.execute();
            }*/
            conn.commit();
        }
        catch (SQLException e)
        {
            conn.rollback();
        }
        finally {
            conn.setAutoCommit(true);
        }
    }

    /**
     * @param driver
     * @param url
     * @param korisnik
     * @param sifra
     * @return Connection on MySQL database
     */
    private Connection connect(String driver, String url, String korisnik,
                               String sifra) {
        Connection konekcija = null;
        try {
            Class.forName(driver);
            konekcija = DriverManager.getConnection(url, korisnik, sifra);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return konekcija;
    }
    /**
     *
     * Method for getting all users from databse
     * @return Array of users
     */
    @RequestMapping("/list")
    public ArrayList<User> listUsers(){
        return userService.getAllUsers();
    }

    @PostMapping("/add/{id}")
    public ResponseEntity<Object> addUser(@PathVariable int id, @RequestBody User user) {
        User savedUser = userService.save(user);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedUser.getId()).toUri();

        userService.addLog("USER_", id, savedUser.getId(), "INSERT");
        return ResponseEntity.created(location).build();
    }


    /*@PostMapping("/post/{id}")
    public ResponseEntity<Object> updateUser(@RequestParam(value = "user") User user, @PathVariable long id,
                                             @RequestParam(value="files") MultipartFile[] files) {
        User userOptional = userService.findById(id);
        if (userOptional != null)
            return ResponseEntity.ok().build();
        user.setId(id);
        userService.save(user);
        return ResponseEntity.noContent().build();
    }*/

    @PostMapping("/post")
    public ResponseEntity<Object> updateUser(@RequestParam(value = "files", required = false) MultipartFile[] files,
                                             @RequestParam("formDataJson") String formDataJson) throws IOException, JSONException {
        JSONObject jsonObject = new JSONObject(formDataJson);
        User user = new User();
        user.setUsername(jsonObject.getString("username"));
        //user.setId(jsonObject.getLong("id"));
        User savedUser = userService.save(user);
        for(MultipartFile uploadedFile : files) {
            long id = fileService.save(uploadedFile);
            userFileService.save(id, savedUser.getId());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/files/{username}")
    public ArrayList<String> getFiles(@PathVariable String username) {
        ArrayList<User> users = userService.getAllUsers();
        long id = -1;
        for (User u: users
             ) {
            if (u.getUsername().equals(username)) {
                id = u.getId();
                break;
            }
        }
        ArrayList<UserFiles> uf = userFileService.getAllUserFiles(id);
        ArrayList<String> files = new ArrayList<String>();
        for (UserFiles userFile: uf
             ) {
            files.add(fileService.getFile(userFile.getFile_id()).get().getFilename());
        }
        return files;
    }

    @PostMapping("/files")
    public ResponseEntity<ByteArrayResource> getFile(@RequestParam("username") String username,
                                                     @RequestParam("filename") String filename) {
        ArrayList<User> users = userService.getAllUsers();
        long id = -1;
        for (User u: users
                ) {
            if (u.getUsername().equals(username)) {
                id = u.getId();
                break;
            }
        }
        ArrayList<UserFiles> uf = userFileService.getAllUserFiles(id);
        File file = new File();
        for (UserFiles userFile: uf
                ) {
            file = fileService.getFile(userFile.getFile_id()).get();
            if(file.getFilename().equals(filename)) {
                ByteArrayResource resource = new ByteArrayResource(file.getFile());
                MediaType mediaType = MediaType.parseMediaType(file.getFiletype());
                return ResponseEntity.ok()
                        .header("X-Content-Type-Options", "nosniff")
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getFilename())
                        .header("Access-Control-Expose-Headers", "Content-Disposition")
                        .contentType(mediaType)
                        .contentLength(file.getFile().length) //
                        .body(resource);
            }
        }
        return null;
    }

    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
