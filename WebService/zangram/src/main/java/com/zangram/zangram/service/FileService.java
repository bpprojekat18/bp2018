package com.zangram.zangram.service;

import com.zangram.zangram.model.File;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

@Service
public interface FileService {
    public ArrayList<File> getAllFiles();
    public long save(MultipartFile file) throws IOException;
    public Optional<File> getFile(long id);
}
