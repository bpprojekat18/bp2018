package com.zangram.zangram.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="location_files")
public class LocationFiles implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="location_files_seq")
    @SequenceGenerator(name="location_files_seq", sequenceName="location_files_seq", allocationSize=1)
    private long id;

    @Column(name = "location_id")
    private long location_id;

    @Column(name = "file_id")
    private long file_id;

    public LocationFiles() {
    }

    public LocationFiles(long locid, long fid) {
        location_id = locid;
        file_id = fid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(long location_id) {
        this.location_id = location_id;
    }

    public long getFile_id() {
        return file_id;
    }

    public void setFile_id(long file_id) {
        this.file_id = file_id;
    }
}
