package com.zangram.zangram.repositories;

import com.zangram.zangram.model.UserPhoto;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.ArrayList;

/**
 * Repository for crud operations for userPhoto table from database
 */
public interface UserPhotoRepository extends MongoRepository<UserPhoto, String> {
    UserPhoto findBy_id(ObjectId _id);
    ArrayList<UserPhoto> findAll();
}
