package com.zangram.zangram.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class UserPhoto {
    @Id
    private ObjectId _id;

    private String userId;
    private String photoId;

    public UserPhoto(ObjectId _id, String userId, String photoId) {
        this._id = _id;
        this.userId = userId;
        this.photoId = photoId;
    }

    public UserPhoto() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }
}
