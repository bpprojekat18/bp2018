package com.zangram.zangram.repositories;

import com.zangram.zangram.model.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for crud operations for location table from database
 */
@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
}
