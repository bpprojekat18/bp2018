package com.zangram.zangram.service;

import com.zangram.zangram.model.Like;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface LikeService {
    public ArrayList<Like> getAllLikes();
    public Optional<Like> getLike(long id);
    public Like save(Like like);
    public Optional<Like> findById(long id);
}
