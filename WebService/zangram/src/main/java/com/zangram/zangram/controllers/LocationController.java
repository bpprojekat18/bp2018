package com.zangram.zangram.controllers;

import com.zangram.zangram.model.*;
import com.zangram.zangram.service.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/locations")
public class LocationController {
    private LocationService locationService;
    private FileService fileService;
    private LocationFilesService locationFileService;

    @Autowired
    public void setProductService(LocationService locationService) {
        this.locationService = locationService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @Autowired
    public void setLocationFileService(@Qualifier("locationFilesServiceImpl") LocationFilesService locationFileService) {this.locationFileService = locationFileService; }
    /**
     *
     * Method for getting all locations from databse
     * @return Array of locations
     */
    @RequestMapping("/list")
    public ArrayList<Location> listLocations(){
        return locationService.getAllLocations();
    }

    @PostMapping("/addl/{id}")
    public ResponseEntity<Object> addLocation(@PathVariable int id, @RequestBody Location location) {
        Location savedLocation = locationService.save(location);

        URI locationurl = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedLocation.getId()).toUri();
        locationService.addLog("LOCATION", id, savedLocation.getId(), "INSERT");
        return ResponseEntity.created(locationurl).build();

    }

    @PostMapping("/post")
    public ResponseEntity<Object> updateLocation(@RequestParam(value = "files", required = false) MultipartFile[] files,
                                             @RequestParam("formDataJson") String formDataJson) throws IOException, JSONException {
        JSONObject jsonObject = new JSONObject(formDataJson);
        Location location = new Location();
        location.setPlace(jsonObject.getString("place"));
        Location savedLocation = locationService.save(location);
        for(MultipartFile uploadedFile : files) {
            long id = fileService.save(uploadedFile);
            locationFileService.save(id, savedLocation.getId());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/files/{place}")
    public ArrayList<String> getFiles(@PathVariable String place) {
        ArrayList<Location> locations = locationService.getAllLocations();
        long id = -1;
        for (Location l: locations) {
            if (l.getPlace().equals(place)) {
                id = l.getId();
                break;
            }
        }
        ArrayList<LocationFiles> locf = locationFileService.getAllLocationFiles(id);
        ArrayList<String> files = new ArrayList<String>();
        for (LocationFiles locationFile: locf) {
            files.add(fileService.getFile(locationFile.getFile_id()).get().getFilename());
        }
        return files;
    }

    @PostMapping("/files")
    public ResponseEntity<ByteArrayResource> getFile(@RequestParam("place") String place,
                                                     @RequestParam("filename") String filename) {
        ArrayList<Location> locations = locationService.getAllLocations();
        long id = -1;
        for (Location l: locations) {
            if (l.getPlace().equals(place)) {
                id = l.getId();
                break;
            }
        }
        ArrayList<LocationFiles> lf = locationFileService.getAllLocationFiles(id);
        File file = new File();
        for (LocationFiles locationFile: lf) {
            file = fileService.getFile(locationFile.getFile_id()).get();
            if(file.getFilename().equals(filename)) {
                ByteArrayResource resource = new ByteArrayResource(file.getFile());
                MediaType mediaType = MediaType.parseMediaType(file.getFiletype());
                return ResponseEntity.ok()
                        .header("X-Content-Type-Options", "nosniff")
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getFilename())
                        .header("Access-Control-Expose-Headers", "Content-Disposition")
                        .contentType(mediaType)
                        .contentLength(file.getFile().length) //
                        .body(resource);
            }
        }
        return null;
    }

    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
