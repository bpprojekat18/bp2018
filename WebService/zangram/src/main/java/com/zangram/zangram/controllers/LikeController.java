package com.zangram.zangram.controllers;

import com.zangram.zangram.model.*;
import com.zangram.zangram.service.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/likes")
public class LikeController {
    private LikeService likeService;
    private FileService fileService;
    private LikeFilesService likeFileService;

    @Autowired
    public void setProductService(LikeService likeService) {
        this.likeService = likeService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @Autowired
    public void setLikeFileService(@Qualifier("likeFilesServiceImpl") LikeFilesService likeFileService) {this.likeFileService = likeFileService; }
    /**
     *
     * Method for getting all likes from databse
     * @return Array of likes
     */
    @RequestMapping("/list")
    public ArrayList<Like> listLikes(){
        return likeService.getAllLikes();
    }

    @PostMapping("/add2")
    public ResponseEntity<Object> addLike(@RequestBody Like like) {
        Like savedLike = likeService.save(like);

        URI likeurl = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedLike.getId()).toUri();

        return ResponseEntity.created(likeurl).build();

    }

    @PostMapping("/post")
    public ResponseEntity<Object> updateLike(@RequestParam(value = "files", required = false) MultipartFile[] files,
                                                 @RequestParam("formDataJson") String formDataJson) throws IOException, JSONException {
        JSONObject jsonObject = new JSONObject(formDataJson);
        Like like = new Like();
        like.setLikerId(jsonObject.getString("like1"));
        Like savedLike = likeService.save(like);
        for(MultipartFile uploadedFile : files) {
            long id = fileService.save(uploadedFile);
            likeFileService.save(id, savedLike.getId());
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/files/{like1}")
    public ArrayList<String> getFiles(@PathVariable String like1) {
        ArrayList<Like> likes = likeService.getAllLikes();
        long id = -1;
        for (Like l: likes) {
            if (l.getLikerId().equals(like1)) {
                id = l.getId();
                break;
            }
        }
        ArrayList<LikeFiles> likf = likeFileService.getAllLikeFiles(id);
        ArrayList<String> files = new ArrayList<String>();
        for (LikeFiles likeFile: likf) {
            files.add(fileService.getFile(likeFile.getFile_id()).get().getFilename());
        }
        return files;
    }

    @PostMapping("/files")
    public void getFile(@RequestParam("like1") String like1, @RequestParam("filename") String filename) {
        ArrayList<Like> likes = likeService.getAllLikes();
        long id = -1;
        for (Like l: likes) {
            if (l.getLikerId().equals(like1)) {
                id = l.getId();
                break;
            }
        }
        ArrayList<LikeFiles> lf = likeFileService.getAllLikeFiles(id);
        File file = new File();
        for (LikeFiles likeFile: lf) {
            file = fileService.getFile(likeFile.getFile_id()).get();
            if(file.getFilename().equals(filename)) {
                writeBytesToFile(file.getFile(), "C:\\temp\\" + file.getFilename());
                break;
            }
        }
    }
    private static void writeBytesToFile(byte[] bFile, String fileDest) {

        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
