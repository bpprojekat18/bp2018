package com.zangram.zangram.service;

import com.zangram.zangram.model.LocationFiles;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface LocationFilesService {
    public ArrayList<LocationFiles> getAllLocationFiles(long id);
    public LocationFiles save(long fileId, long locationId);
    public ArrayList<LocationFiles> getAll();
}
