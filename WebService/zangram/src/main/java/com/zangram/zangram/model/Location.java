package com.zangram.zangram.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="location")
public class Location implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="location_seq")
    @SequenceGenerator(name="location_seq", sequenceName="location_seq", allocationSize=1)
    private long id;

    @Column(name = "place")
    private String place;

    public Location() {
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
