package com.zangram.zangram.service;

import com.zangram.zangram.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public interface UserService {
    public ArrayList<User> getAllUsers();
    public Optional<User> getUser(long id);
    public User save(User user);
    public Optional<User> findById(long id);
    public void addLog(String table, long userId, long dataId, String job);
}

