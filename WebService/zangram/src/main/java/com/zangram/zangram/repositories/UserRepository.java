package com.zangram.zangram.repositories;

import com.zangram.zangram.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Repository for crud operations for user table from database
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
