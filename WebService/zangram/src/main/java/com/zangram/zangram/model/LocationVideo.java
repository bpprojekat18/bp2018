package com.zangram.zangram.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class LocationVideo {
    @Id
    private ObjectId _id;

    private String locationId;
    private String videoId;

    public LocationVideo(ObjectId _id, String locationId, String videoId) {
        this._id = _id;
        this.locationId = locationId;
        this.videoId = videoId;
    }

    public LocationVideo() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getLocationId() { return locationId; }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
