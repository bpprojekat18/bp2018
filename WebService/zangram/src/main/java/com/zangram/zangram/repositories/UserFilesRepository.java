package com.zangram.zangram.repositories;

import com.zangram.zangram.model.UserFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFilesRepository extends CrudRepository<UserFiles, Long> {
}
