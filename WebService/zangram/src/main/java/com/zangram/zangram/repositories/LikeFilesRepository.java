package com.zangram.zangram.repositories;

import com.zangram.zangram.model.LikeFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeFilesRepository extends CrudRepository<LikeFiles, Long> {
}
