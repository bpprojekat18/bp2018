package com.zangram.zangram.repositories;

import com.zangram.zangram.model.LocationFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationFilesRepository extends CrudRepository<LocationFiles, Long> {
}
