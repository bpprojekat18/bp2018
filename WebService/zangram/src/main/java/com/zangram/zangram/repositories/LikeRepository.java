package com.zangram.zangram.repositories;

import com.zangram.zangram.model.Like;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for crud operations for like table from database
 */
@Repository
public interface LikeRepository extends CrudRepository<Like, Long> {
}
