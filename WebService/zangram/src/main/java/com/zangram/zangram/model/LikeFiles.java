package com.zangram.zangram.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="like_files")
public class LikeFiles implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="like_files_seq")
    @SequenceGenerator(name="like_files_seq", sequenceName="like_files_seq", allocationSize=1)
    private long id;

    @Column(name = "like_id")
    private long like_id;

    @Column(name = "file_id")
    private long file_id;

    public LikeFiles() {
    }

    public LikeFiles(long lid, long fid) {
        like_id = lid;
        file_id = fid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLike_id() {
        return like_id;
    }

    public void setLike_id(long like_id) {
        this.like_id = like_id;
    }

    public long getFile_id() {
        return file_id;
    }

    public void setFile_id(long file_id) {
        this.file_id = file_id;
    }
}
