package com.zangram.zangram.repositories;

import com.zangram.zangram.model.LikeText;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LikeTextRepository extends MongoRepository<LikeText, String> {
    LikeText findBy_id(ObjectId _id);
}

