package com.zangram.zangram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZangramApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZangramApplication.class, args);
	}
}
