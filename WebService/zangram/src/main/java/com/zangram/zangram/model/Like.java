package com.zangram.zangram.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="like_")
public class Like implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="like_seq")
    @SequenceGenerator(name="like_seq", sequenceName="like_seq", allocationSize=1)
    private int id;

    @Column(name = "liker_id")
    private String likerId;

    public Like() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLikerId() {
        return likerId;
    }

    public void setLikerId(String likerId) {
        this.likerId = likerId;
    }
}
