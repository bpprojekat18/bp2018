package com.zangram.zangram.service;

import com.zangram.zangram.model.Like;
import com.zangram.zangram.repositories.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class LikeServiceImpl implements LikeService {
    private LikeRepository likeRepository;

    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository) {
        this.likeRepository = likeRepository;
    }

    /**
     * Method for getting all likes from database and adding them into array
     * @return Array of likes
     */
    @Override
    public ArrayList<Like> getAllLikes() {
        ArrayList<Like> likes = new ArrayList<>();
        likeRepository.findAll().forEach(likes::add); //fun with Java 8
        return likes;
    }
    @Override
    public Optional<Like> getLike(long id) {
        return likeRepository.findById(id);
    }

    @Override
    public Like save(Like like) {
        return likeRepository.save(like);
    }

    @Override
    public Optional<Like> findById(long id) {
        return likeRepository.findById(id);
    }
}
