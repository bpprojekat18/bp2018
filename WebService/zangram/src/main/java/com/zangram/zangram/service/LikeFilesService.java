package com.zangram.zangram.service;

import com.zangram.zangram.model.LikeFiles;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface LikeFilesService {
    public ArrayList<LikeFiles> getAllLikeFiles(long id);
    public LikeFiles save(long fileId, long likeId);
}
