package com.zangram.zangram.service;

import com.zangram.zangram.model.User;
import com.zangram.zangram.model.UserFiles;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface UserFilesService {
    public ArrayList<UserFiles> getAllUserFiles(long id);
    public UserFiles save(long fileId, long userId);
    public ArrayList<UserFiles> getAll();
}
