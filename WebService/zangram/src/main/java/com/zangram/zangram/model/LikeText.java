package com.zangram.zangram.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class LikeText {
    @Id
    private ObjectId _id;

    private String likeId;
    private String textfileId;

    public LikeText(ObjectId _id, String likeId, String textfileId) {
        this._id = _id;
        this.likeId = likeId;
        this.textfileId = textfileId;
    }

    public LikeText() {

    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getLikeId() {
        return likeId;
    }

    public void setLikeId(String likeId) {
        this.likeId = likeId;
    }

    public String getTextfileId() {
        return textfileId;
    }

    public void setTextfileId(String textfileId) {
        this.textfileId = textfileId;
    }
}
