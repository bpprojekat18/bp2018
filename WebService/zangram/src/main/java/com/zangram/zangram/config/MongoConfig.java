package com.zangram.zangram.config;

import com.mongodb.DB;
import com.mongodb.client.MongoDatabase;
import com.mongodb.gridfs.GridFS;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

//configuration class for mongo database

/**
 * Configuration class for mongo database
 */
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${spring.data.mongodb.host}")
    private String host;
    @Value("${spring.data.mongodb.database}")
    private String database;

    @Override
    protected String getDatabaseName() {
        return database;
    }

    /*@Override
    public Mongo mongo() throws Exception {
        return new MongoClient(host);
    }*/

    @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {
        return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
    }

    @Override
    public MongoClient mongoClient() {
        return null;
    }
}
