package com.zangram.zangram.service;

import com.zangram.zangram.model.LocationFiles;
import com.zangram.zangram.model.User;
import com.zangram.zangram.repositories.LocationFilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LocationFilesServiceImpl implements LocationFilesService {
    private LocationFilesRepository locationFilesRepository;

    @Autowired
    public LocationFilesServiceImpl(LocationFilesRepository locationFilesRepository) {
        this.locationFilesRepository = locationFilesRepository;
    }
    @Override
    public ArrayList<LocationFiles> getAllLocationFiles(long id) {
        ArrayList<LocationFiles> locationFiles = new ArrayList<>();
        ArrayList<LocationFiles> all = (ArrayList<LocationFiles>)locationFilesRepository.findAll();
        for (LocationFiles uf: all
        ) {
            if (uf.getLocation_id() == id)
                locationFiles.add(uf);
        }
        return locationFiles;
    }

    @Override
    public LocationFiles save(long fileId, long locationId) {
        LocationFiles locfile = new LocationFiles(locationId, fileId);
        locfile.setId(1);
        return locationFilesRepository.save(locfile);
    }

    @Override
    public ArrayList<LocationFiles> getAll() {
        ArrayList<LocationFiles> locatonFiles = new ArrayList<>();
        locationFilesRepository.findAll().forEach(locatonFiles::add); //fun with Java 8
        return locatonFiles;
    }
}
