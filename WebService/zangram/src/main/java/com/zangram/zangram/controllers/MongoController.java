package com.zangram.zangram.controllers;

import java.io.*;

import com.mongodb.DB;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.gridfs.GridFS;
import com.zangram.zangram.model.LikeText;
import com.zangram.zangram.model.LocationVideo;
import com.zangram.zangram.model.UserPhoto;
import com.zangram.zangram.repositories.LikeTextRepository;
import com.zangram.zangram.repositories.LocationVideoRepository;
import com.zangram.zangram.repositories.UserPhotoRepository;
import com.zangram.zangram.service.LikeService;
import com.zangram.zangram.service.LocationService;
import com.zangram.zangram.service.UserService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.web.bind.annotation.*;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;

@RestController
@RequestMapping("/mongo")
public class MongoController {

    @Autowired
    private GridFsOperations gridFsOperations;

    @Autowired
    private UserPhotoRepository repository;

    private UserService userService;

    @Autowired
    private LocationVideoRepository repositoryLocation;

    private LocationService locationService;

    @Autowired
    private LikeTextRepository repositoryLike;

    private LikeService likeService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    String fileId = "";

    /**
     * Saving image/video/file... in MongoDB
     * @return Mesagge:"Files stored successfully..."
     * @throws FileNotFoundException
     */
    @GetMapping("/saveFiles")
    public String saveFile() throws FileNotFoundException {
        // define metadata
        //DBObject metaData = new BasicDBObject();
        //metaData.put("organization", "zangram");

        // store image file
        //InputStream inputStream = new FileInputStream("C:/Users/zlayaa/Desktop/logo.png");
        //metaData.put("type", "image");

        //fileId = gridFsOperations.store(inputStream, "logo.png", "image/png", metaData).getId().toString();
        //repository.insert(new UserPhoto(new ObjectId(), "4", fileId));
        //System.out.println("File id stored : " + fileId);

        // add code to store .txt file
        //DBObject metaData = new BasicDBObject();
        //metaData.put("organization", "zangram");

        //store txt file
        //InputStream inputStream = new FileInputStream("C:/Users/Armin/Desktop/textfile.txt");
        //metaData.put("type", "file");

        //fileId = gridFsOperations.store(inputStream, "textfile.txt", "file/txt", metaData).getId().toString();
        //repositoryLike.insert(new LikeText(new ObjectId(), "1", fileId));
        //System.out.println("File id stored : " + fileId);


        // add code to store video file
        //define metadata
        //DBObject metaData = new BasicDBObject();
        //metaData.put("organization", "zangram");

        //store image file
        //InputStream inputStream = new FileInputStream("C:/Users/Armin/Desktop/video.mp4");
        //metaData.put("type", "video");

        //fileId = gridFsOperations.store(inputStream, "video.mp4", "video/mp4", metaData).getId().toString();
        //repositoryLocation.insert(new LocationVideo(new ObjectId(), "1", fileId));
        //System.out.println("File id stored : " + fileId);

        return "Files stored successfully...";
    }

    /**
     * Method for getting image from MongoDB with image id
     * @param id ID of image that we want to get
     * @return Returning photo from database
     * @throws IOException
     */
    @GetMapping("/retrive/image/{id}")
    public UserPhoto retriveImageFile(@PathVariable("id") ObjectId id) throws IOException {
        //5bdf859485cdd513ac3fb02b
        UserPhoto up = repository.findBy_id(id);
        fileId = up.getPhotoId();
        //GridFSDBFile dbFile = gridFsOperations.findOne(new Query(Criteria.where("_id").is(fileId)));
        //dbFile.writeTo("C:/Users/zlayaa/Desktop/reactive-logo.png");
        //dbFile.writeTo("C:/Users/Armin/Desktop/reactive-logo.png");
        return up;
    }

    /**
     * Method for getting document from MongoDB with document id
     * @return
     * @throws IOException
     */
    // implement this method
    //5be22c86663bc011f8d2e2f0
    @GetMapping("/retrive/document/{id}")
    public LikeText retriveTxtFile(@PathVariable("id") ObjectId id) throws IOException {
        LikeText lt = repositoryLike.findBy_id(id);
        fileId = lt.getTextfileId();
        //GridFSDBFile dbFile = gridFsOperations.findOne(new Query(Criteria.where("_id").is(fileId)));
        //dbFile.writeTo("C:/Users/zlayaa/Desktop/reactive-logo.png");
        //dbFile.writeTo("C:/Users/Armin/Desktop/gettext.txt");
        return lt;
    }

    /**
     * Method for getting video from MongoDB with video id
     * @param id ID of video that we want to get
     * @return Returning video from database
     * @throws IOException
     */
    //5be09c0d96249d1bc8d68f2d
    @GetMapping("/retrive/video/{id}")
    public LocationVideo retriveVideoFile(@PathVariable("id") ObjectId id) throws IOException {
        LocationVideo lv = repositoryLocation.findBy_id(id);
        fileId = lv.getVideoId();
        //GridFSDBFile dbFile = gridFsOperations.findOne(new Query(Criteria.where("_id").is(fileId)));
        //dbFile.writeTo("C:/Users/Armin/Desktop/videoget.mp4");
        return lv;
    }
}
