package com.zangram.zangram.service;

import com.zangram.zangram.model.Like;
import com.zangram.zangram.model.User;
import com.zangram.zangram.model.UserFiles;
import com.zangram.zangram.repositories.UserFilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserFilesServiceImpl implements UserFilesService {
    private UserFilesRepository userFilesRepository;

    @Autowired
    public UserFilesServiceImpl(UserFilesRepository userFilesRepository) {
        this.userFilesRepository = userFilesRepository;
    }
    @Override
    public ArrayList<UserFiles> getAllUserFiles(long id) {
        ArrayList<UserFiles> userFiles = new ArrayList<>();
        ArrayList<UserFiles> all = (ArrayList<UserFiles>)userFilesRepository.findAll();
        for (UserFiles uf: all
             ) {
            if (uf.getUser_id() == id)
                userFiles.add(uf);
        }
        return userFiles;
    }

    @Override
    public UserFiles save(long fileId, long userId) {
        UserFiles uf = new UserFiles(userId, fileId);
        uf.setId(1);
        return userFilesRepository.save(uf);
    }

    @Override
    public ArrayList<UserFiles> getAll() {
        ArrayList<UserFiles> userFiles = new ArrayList<>();
        userFilesRepository.findAll().forEach(userFiles::add); //fun with Java 8
        return userFiles;
    }
}
