package com.zangram.zangram.service;

import com.zangram.zangram.model.Location;
import com.zangram.zangram.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {
    private LocationRepository locationRepository;

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    /**
     * Method for getting all locations from database and adding them into array
     * @return Array of locations
     */
    @Override
    public ArrayList<Location> getAllLocations() {
        ArrayList<Location> locations = new ArrayList<>();
        locationRepository.findAll().forEach(locations::add); //fun with Java 8
        return locations;
    }
    @Override
    public Optional<Location> getLocation(long id) {
        return locationRepository.findById(id);
    }

    @Override
    public Location save(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Optional<Location> findById(long id) {
        return locationRepository.findById(id);
    }

    @Override
    public void addLog(String table, long userId, long dataId, String job) {
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("LOGS_PROCEDURE");

        //Declare the parameters in the same order
        query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, Long.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(3, Long.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);

        //Pass the parameter values
        query.setParameter(1, table);
        query.setParameter(2, userId);
        query.setParameter(3, dataId);
        query.setParameter(4, job);

        //Execute query
        query.execute();
    }
}
