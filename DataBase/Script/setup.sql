--------------------------------------------------------
--  File created - Tuesday-October-30-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table FOLLOWER
--------------------------------------------------------

  CREATE TABLE "BP18"."FOLLOWER" 
   (	"ID" NUMBER(10,0), 
	"FOLLOWER_ID" NUMBER(10,0), 
	"FOLLOWING_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table MUSIC
--------------------------------------------------------

  CREATE TABLE "BP18"."MUSIC" 
   (	"ID" NUMBER(10,0), 
	"CAPTION" VARCHAR2(45 BYTE), 
	"MUSIC" BLOB
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" 
 LOB ("MUSIC") STORE AS BASICFILE (
  TABLESPACE "ETFLAB" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING ) ;
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "BP18"."COMMENTS" 
   (	"ID" NUMBER(10,0), 
	"POST_ID" NUMBER(10,0), 
	"COMMENT_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table POST
--------------------------------------------------------

  CREATE TABLE "BP18"."POST" 
   (	"ID" NUMBER(10,0), 
	"DESCRIPTION" VARCHAR2(45 BYTE), 
	"VIDEO_ID" NUMBER(10,0), 
	"PHOTO_ID" NUMBER(10,0), 
	"MUSIC_ID" NUMBER(10,0), 
	"LOCATION_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table LIKE_
--------------------------------------------------------

  CREATE TABLE "BP18"."LIKE_" 
   (	"ID" NUMBER(10,0), 
	"LIKER_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table PHOTO
--------------------------------------------------------

  CREATE TABLE "BP18"."PHOTO" 
   (	"ID" NUMBER(10,0), 
	"CAPTION" VARCHAR2(45 BYTE), 
	"PHOTO" BLOB
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" 
 LOB ("PHOTO") STORE AS BASICFILE (
  TABLESPACE "ETFLAB" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING ) ;
--------------------------------------------------------
--  DDL for Table LIKES
--------------------------------------------------------

  CREATE TABLE "BP18"."LIKES" 
   (	"ID" NUMBER(10,0), 
	"POST_ID" NUMBER(10,0), 
	"LIKE_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table LOCATION
--------------------------------------------------------

  CREATE TABLE "BP18"."LOCATION" 
   (	"ID" NUMBER(10,0), 
	"PLACE" VARCHAR2(45 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table COMMENT_
--------------------------------------------------------

  CREATE TABLE "BP18"."COMMENT_" 
   (	"ID" NUMBER(10,0), 
	"TEXT" VARCHAR2(45 BYTE), 
	"COMMENTER_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table TAGGED_POSTS
--------------------------------------------------------

  CREATE TABLE "BP18"."TAGGED_POSTS" 
   (	"ID" NUMBER(10,0), 
	"POST_ID" NUMBER(10,0), 
	"TAGGED_POST_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table TAGGED_POST
--------------------------------------------------------

  CREATE TABLE "BP18"."TAGGED_POST" 
   (	"ID" NUMBER(10,0), 
	"TAGGED_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table USER_
--------------------------------------------------------

  CREATE TABLE "BP18"."USER_" 
   (	"ID" NUMBER(10,0), 
	"FULLNAME" VARCHAR2(45 BYTE), 
	"USERNAME" VARCHAR2(45 BYTE), 
	"PASSWORD" VARCHAR2(45 BYTE), 
	"EMAIL" VARCHAR2(45 BYTE), 
	"BIRTHDAY" TIMESTAMP (0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Table VIDEO
--------------------------------------------------------

  CREATE TABLE "BP18"."VIDEO" 
   (	"ID" NUMBER(10,0), 
	"CAPTION" VARCHAR2(45 BYTE), 
	"VIDEO" BLOB
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" 
 LOB ("VIDEO") STORE AS BASICFILE (
  TABLESPACE "ETFLAB" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING ) ;
REM INSERTING into BP18.FOLLOWER
SET DEFINE OFF;
REM INSERTING into BP18.MUSIC
SET DEFINE OFF;
REM INSERTING into BP18.COMMENTS
SET DEFINE OFF;
REM INSERTING into BP18.POST
SET DEFINE OFF;
REM INSERTING into BP18.LIKE_
SET DEFINE OFF;
REM INSERTING into BP18.PHOTO
SET DEFINE OFF;
REM INSERTING into BP18.LIKES
SET DEFINE OFF;
REM INSERTING into BP18.LOCATION
SET DEFINE OFF;
Insert into BP18.LOCATION (ID,PLACE) values (1,'Madrid');
REM INSERTING into BP18.COMMENT_
SET DEFINE OFF;
REM INSERTING into BP18.TAGGED_POSTS
SET DEFINE OFF;
REM INSERTING into BP18.TAGGED_POST
SET DEFINE OFF;
REM INSERTING into BP18.USER_
SET DEFINE OFF;
Insert into BP18.USER_ (ID,FULLNAME,USERNAME,PASSWORD,EMAIL,BIRTHDAY) values (4,'test','test','test','test',to_timestamp('01-FEB-01 12.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
REM INSERTING into BP18.VIDEO
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index SYS_C00233116
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233116" ON "BP18"."FOLLOWER" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index FOLLOWER_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."FOLLOWER_ID_IDX" ON "BP18"."FOLLOWER" ("FOLLOWER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index FOLLOWING_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."FOLLOWING_ID_IDX" ON "BP18"."FOLLOWER" ("FOLLOWING_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233089
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233089" ON "BP18"."MUSIC" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233122
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233122" ON "BP18"."COMMENTS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index POST_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."POST_ID_IDX" ON "BP18"."COMMENTS" ("POST_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index COMMENT_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."COMMENT_ID_IDX" ON "BP18"."COMMENTS" ("COMMENT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233094
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233094" ON "BP18"."POST" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index VIDEO_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."VIDEO_ID_IDX" ON "BP18"."POST" ("VIDEO_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index MUDISC_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."MUDISC_ID_IDX" ON "BP18"."POST" ("MUSIC_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index PHOTO_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."PHOTO_ID_IDX" ON "BP18"."POST" ("PHOTO_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index LOCATION_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."LOCATION_ID_IDX" ON "BP18"."POST" ("LOCATION_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233107
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233107" ON "BP18"."LIKE_" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index LIKER_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."LIKER_ID_IDX" ON "BP18"."LIKE_" ("LIKER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233087
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233087" ON "BP18"."PHOTO" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233136
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233136" ON "BP18"."LIKES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index LIKE_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."LIKE_ID_IDX" ON "BP18"."LIKES" ("LIKE_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index POSTS_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."POSTS_ID_IDX" ON "BP18"."LIKES" ("POST_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233091
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233091" ON "BP18"."LOCATION" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233103
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233103" ON "BP18"."COMMENT_" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index COMMENTER_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."COMMENTER_ID_IDX" ON "BP18"."COMMENT_" ("COMMENTER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233146
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233146" ON "BP18"."TAGGED_POSTS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index TAGGED_POST__ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."TAGGED_POST__ID_IDX" ON "BP18"."TAGGED_POSTS" ("POST_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index TAGGED_POST_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."TAGGED_POST_ID_IDX" ON "BP18"."TAGGED_POSTS" ("TAGGED_POST_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233111
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233111" ON "BP18"."TAGGED_POST" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index TAGGED_ID_IDX
--------------------------------------------------------

  CREATE INDEX "BP18"."TAGGED_ID_IDX" ON "BP18"."TAGGED_POST" ("TAGGED_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233100
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233100" ON "BP18"."USER_" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  DDL for Index SYS_C00233093
--------------------------------------------------------

  CREATE UNIQUE INDEX "BP18"."SYS_C00233093" ON "BP18"."VIDEO" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB" ;
--------------------------------------------------------
--  Constraints for Table FOLLOWER
--------------------------------------------------------

  ALTER TABLE "BP18"."FOLLOWER" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."FOLLOWER" MODIFY ("FOLLOWER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."FOLLOWER" MODIFY ("FOLLOWING_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."FOLLOWER" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table MUSIC
--------------------------------------------------------

  ALTER TABLE "BP18"."MUSIC" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."MUSIC" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMMENTS
--------------------------------------------------------

  ALTER TABLE "BP18"."COMMENTS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."COMMENTS" MODIFY ("POST_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."COMMENTS" MODIFY ("COMMENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."COMMENTS" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table POST
--------------------------------------------------------

  ALTER TABLE "BP18"."POST" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table LIKE_
--------------------------------------------------------

  ALTER TABLE "BP18"."LIKE_" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."LIKE_" MODIFY ("LIKER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."LIKE_" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PHOTO
--------------------------------------------------------

  ALTER TABLE "BP18"."PHOTO" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."PHOTO" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table LIKES
--------------------------------------------------------

  ALTER TABLE "BP18"."LIKES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."LIKES" MODIFY ("POST_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."LIKES" MODIFY ("LIKE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."LIKES" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table LOCATION
--------------------------------------------------------

  ALTER TABLE "BP18"."LOCATION" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."LOCATION" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMMENT_
--------------------------------------------------------

  ALTER TABLE "BP18"."COMMENT_" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."COMMENT_" MODIFY ("COMMENTER_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."COMMENT_" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table TAGGED_POSTS
--------------------------------------------------------

  ALTER TABLE "BP18"."TAGGED_POSTS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."TAGGED_POSTS" MODIFY ("POST_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."TAGGED_POSTS" MODIFY ("TAGGED_POST_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."TAGGED_POSTS" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table TAGGED_POST
--------------------------------------------------------

  ALTER TABLE "BP18"."TAGGED_POST" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."TAGGED_POST" MODIFY ("TAGGED_ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."TAGGED_POST" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table USER_
--------------------------------------------------------

  ALTER TABLE "BP18"."USER_" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."USER_" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ETFLAB"  ENABLE;
--------------------------------------------------------
--  Constraints for Table VIDEO
--------------------------------------------------------

  ALTER TABLE "BP18"."VIDEO" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "BP18"."VIDEO" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOCOMPRESS LOGGING
  TABLESPACE "ETFLAB"  ENABLE;
